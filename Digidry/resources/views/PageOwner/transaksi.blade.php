@extends('layouts.appOwner')
@section('content')
<div class="cotainer">
<div class="card p-5">
<h2><strong>Data Transaksi</strong></h2>
<hr>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                             @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                             @endforeach
                             </ul>
                        </div>
                    @endif
                    <a href="{{ url('/owner/transaksi/laporan') }}" class="btn btn-primary" display="inline-block">Generate Laporan</a>
                    <hr>
<table id="datatable" class="table table-bordered" style="width:100%">
                <thead>
                  <tr>
                    <th class="text-center">Nama Outlet</th>
                    <th class="text-center">Nama Pelanggan</th>
                    <th class="text-center">Nama Paket</th>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Berat</th>
                    <th class="text-center">Biaya Tambahan</th>
                    <th class="text-center">Harga Total</th>
                    <th class="text-center">Proses</th>
                    <th class="text-center">Status Pembayaran</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($transaksi as $item)
                        <tr>
                        <td class="text-center">{{ $item->nama_outlet }}</td>
                        <td class="text-center">{{ $item->nama_pelanggan }}</td>
                        <td class="text-center">{{ $item->nama_paket }}</td>
                        <td class="text-center">{{ $item->tanggal_bayar }}</td>
                        <td class="text-center">{{ $item->berat }}</td>
                        <td class="text-center">{{ $item->biaya_tambahan }}</td>
                        <td class="text-center">{{ $item->harga_total }}</td>
                        <td class="text-center">{{ $item->nama_proses }}</td>
                        <td class="text-center">{{ $item->nama_status }}</td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
    </div>
</div>
@endsection
