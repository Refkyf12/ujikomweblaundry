@extends('layouts.app')
@section('content')
<body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                <strong>Tambah Proses</strong>
                </div>
                <div class="card-body">
                    <a href="/admin/proses" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    
                    <form method="post" action="/admin/proses/store">
 
                        {{ csrf_field() }}
 
                        <div class="form-group">
                            <label>Nama Proses</label>
                            <input type="text" name="nama_proses" class="form-control" placeholder="Nama proses ..">
 
                            @if($errors->has('nama_proses'))
                                <div class="text-danger">
                                    {{ $errors->first('nama_proses')}}
                                </div>
                            @endif
 
                        </div>

                        <div class="form-group">
                            <label>Urutan</label>
                            <input type="text" name="urutan" class="form-control" placeholder="Urutan ..">
 
                            @if($errors->has('urutan'))
                                <div class="text-danger">
                                    {{ $errors->first('urutan')}}
                                </div>
                            @endif
 
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
 
                    </form>
 
                </div>
            </div>
        </div>
    </body>
    @endsection 