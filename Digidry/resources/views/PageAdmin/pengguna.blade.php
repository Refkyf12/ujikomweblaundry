@extends('layouts.app')
@section('content')
<div class="cotainer">
<div class="card p-5">
<h2><strong>Data Pengguna</strong></h2>
<a href="/admin/pengguna/tambah" class="btn btn-primary" display="inline-block">Tambah Pengguna</a>
<hr>
<table id="datatable" class="table table-bordered" style="width:100%">
                <thead>
                  <tr>
                    <th class="text-center">Nama Pengguna</th>
                    <th class="text-center">Alamat E-mail</th>
                    <th class="text-center">Role</th>
                    <th class="text-center">Opsi</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($pengguna as $item)
                        <tr>
                        <td class="text-center">{{ $item->name }}</td>
                        <td class="text-center">{{ $item->email }}</td>
                        <td class="text-center">{{ $item->role }}</td>
                        <td class="text-center">
                            <a href="/admin/pengguna/edit/{{ $item->id }}" class="btn btn-warning">Edit</a>
                            <a href="/admin/pengguna/hapus/{{ $item->id }}" class="btn btn-danger">Hapus</a>
                        </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
    </div>
</div>
@endsection
