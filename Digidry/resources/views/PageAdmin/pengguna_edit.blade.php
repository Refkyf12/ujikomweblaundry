@extends('layouts.app')
@section('content')
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    <strong>Edit Data</strong>
                </div>
                <div class="card-body">
                    <a href="/admin/pengguna" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    
 
                    <form method="post" action="/admin/pengguna/update/{{ $pengguna->id }}">
 
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
 
                        <div class="form-group">
                            <label>Nama Pelanggan</label>
                            <input type="text" name="name" class="form-control" placeholder="Nama pengguna .." value=" {{ $pengguna->name }} ">
 
                            @if($errors->has('name'))
                                <div class="text-danger">
                                    {{ $errors->first('name')}}
                                </div>
                            @endif
 
                        </div>

                        <div class="form-group">
                            <label>Alamat E-mail</label>
                            <input type="text" name="email" class="form-control" placeholder="Alamat E-mail .."  value=" {{ $pengguna->email }} ">
 
                            @if($errors->has('email'))
                                <div class="text-danger">
                                    {{ $errors->first('email')}}
                                </div>
                            @endif
 
                        </div>
 
                        <div class="form-group">
                            <label for="role">Role</label>
                             <select class="form-control" name="role" id="role"  value=" {{ $pengguna->role }} ">
                                <option value= "1" {{ old('role') == '1' ? 'selected' : '' }} >1</option>
                                <option value= "2" {{ old('role') == '2' ? 'selected' : '' }} >2</option>
                                <option value= "3" {{ old('role') == '3' ? 'selected' : '' }} >3</option>
                            </select     
                         </div>
                         <label for="role">Note:<br> 1 = Admin<br> 2 = Kasir<br> 3 = Owner</label>

                         <div class="form-group">
                            <label>Passsword</label>
                            <input type="password" name="password" class="form-control" placeholder="Password .."  value=" {{ $pengguna->password }} ">
 
                            @if($errors->has('password'))
                                <div class="text-danger">
                                    {{ $errors->first('password')}}
                                </div>
                            @endif
 
                        </div>

                        <div class="form-group">
                            <label for="password-confirm">Konfirmasi Password</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" value=" {{ $pengguna->password }}">
                            </div>
                        </div>
 
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
 
                    </form>
 
                </div>
            </div>
        </div>
    </body>
@endsection