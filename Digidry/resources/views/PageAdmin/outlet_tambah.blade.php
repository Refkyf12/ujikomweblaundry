@extends('layouts.app')
@section('content')
<body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                <strong>Tambah Outlet</strong>
                </div>
                <div class="card-body">
                    <a href="/admin/outlet" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    
                    <form method="post" action="/admin/outlet/store">
 
                        {{ csrf_field() }}
 
                        <div class="form-group">
                            <label>Nama Outlet</label>
                            <input type="text" name="nama_outlet" class="form-control" placeholder="Nama outlet ..">
 
                            @if($errors->has('nama_outlet'))
                                <div class="text-danger">
                                    {{ $errors->first('nama_outlet')}}
                                </div>
                            @endif
 
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" name="alamat" class="form-control" placeholder="Alamat ..">
 
                            @if($errors->has('alamat'))
                                <div class="text-danger">
                                    {{ $errors->first('alamat')}}
                                </div>
                            @endif
 
                        </div>

                         <div class="form-group">
                            <label>Nomor Telepon</label>
                            <input type="text" name="no_telp" class="form-control" placeholder="Nomor Telepon ..">
 
                            @if($errors->has('no_telp'))
                                <div class="text-danger">
                                    {{ $errors->first('no_telp')}}
                                </div>
                            @endif
 
                        </div>
 
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
 
                    </form>
 
                </div>
            </div>
        </div>
    </body>
    @endsection