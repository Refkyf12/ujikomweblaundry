@extends('layouts.app')
@section('content')
<body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                <strong>Tambah Status</strong>
                </div>
                <div class="card-body">
                    <a href="/admin/status" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    
                    <form method="post" action="/admin/status/store">
 
                        {{ csrf_field() }}
 
                        <div class="form-group">
                            <label>Nama Status</label>
                            <input type="text" name="nama_status" class="form-control" placeholder="Nama status ..">
 
                            @if($errors->has('nama_status'))
                                <div class="text-danger">
                                    {{ $errors->first('nama_status')}}
                                </div>
                            @endif
 
                        </div>

                        <div class="form-group">
                            <label>Urutan</label>
                            <input type="text" name="urutan" class="form-control" placeholder="Urutan ..">
 
                            @if($errors->has('urutan'))
                                <div class="text-danger">
                                    {{ $errors->first('urutan')}}
                                </div>
                            @endif
 
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
 
                    </form>
 
                </div>
            </div>
        </div>
    </body>
    @endsection 