@extends('layouts.app')
@section('content')
<body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                <strong>Manage Data Paket Cucian</strong>
                </div>
                <div class="card-body">
                    <a href="/admin/paketcucian" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    <form action=" {{ url('/admin/paketcucian' ,@$paketcucian->id_paket) }} " method="POST">
 
                    {{ csrf_field() }} 
 
                        <div class="form-group">
                            <label for="nama_outlet">Nama Outlet</label>
                             <select class="form-control" name="id_outlet" id="id_outlet">
                             @foreach($combo as $row)
                                <option value="{{ old('id_outlet' ,@$row->id) }}">{{ @$row->nama_outlet }}</option>
                            @endforeach
                            </select>
                         </div>

                        <div class="form-group">
                            <label>Jenis</label>
                            <input type="text" name="jenis" class="form-control" placeholder="Jenis .." value=" {{ old('jenis', @$paketcucian->jenis) }} ">
 
 
                        </div>

                         <div class="form-group">
                            <label>Nama Paket</label>
                            <input type="text" name="nama_paket" class="form-control" placeholder="Nama Paket .." value=" {{ old('nama_paket', @$paketcucian->nama_paket) }} ">

 
                        </div>

                        <div class="form-group">
                            <label>Harga</label>
                            <input type="text" name="harga" class="form-control" placeholder="Harga .." value=" {{ old('harga', @$paketcucian->harga) }} ">
 
 
                        </div>
 
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="submit">
                        </div>
 
                    </form>
 
                </div>
            </div>
        </div>
    </body>
    @endsection