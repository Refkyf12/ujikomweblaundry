@extends('layouts.app')
@section('content')
<div class="cotainer">
<div class="card p-5">
<h2><strong>Data Proses</strong></h2>
<a href="/admin/proses/tambah" class="btn btn-primary" display="inline-block">Tambah Proses</a>
<hr>
<table id="datatable" class="table table-bordered" style="width:100%">
                <thead>
                  <tr>
                    <th class="text-center">Nama</th>
                    <th class="text-center">Urutan</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($proses as $item)
                        <tr>
                        <td class="text-center">{{ $item->nama_proses }}</td>
                        <td class="text-center">{{ $item->urutan }}</td>
                        <td class="text-center">
                            <a href="/admin/proses/hapus/{{ $item->id }}" class="btn btn-danger">Hapus</a>
                        </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
    </div>
</div>
@endsection
