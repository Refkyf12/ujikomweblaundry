@extends('layouts.app')
@section('content')
<div class="cotainer">
<div class="card p-5">
<h2><strong>Data Transaksi</strong></h2>
<a href="{{ url('/admin/transaksi/tambah') }}" class="btn btn-primary" display="inline-block">Tambah Transaksi</a><br>
<a href="{{ url('/admin/transaksi/laporan') }}" class="btn btn-primary" display="inline-block">Generate Laporan</a>
<hr>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                             @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                             @endforeach
                             </ul>
                        </div>
                    @endif
<table id="datatable" class="table table-bordered" style="width:100%">
                <thead>
                  <tr>
                    <th class="text-center">Nama Outlet</th>
                    <th class="text-center">Nama Pelanggan</th>
                    <th class="text-center">Nama Paket</th>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Berat</th>
                    <th class="text-center">Biaya Tambahan</th>
                    <th class="text-center">Harga Total</th>
                    <th class="text-center">Proses</th>
                    <th class="text-center">Status Pembayaran</th>
                    <th class="text-center">Opsi</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($transaksi as $item)
                        <tr>
                        <td class="text-center">{{ $item->nama_outlet }}</td>
                        <td class="text-center">{{ $item->nama_pelanggan }}</td>
                        <td class="text-center">{{ $item->nama_paket }}</td>
                        <td class="text-center">{{ $item->tanggal_bayar }}</td>
                        <td class="text-center">{{ $item->berat }}</td>
                        <td class="text-center">{{ $item->biaya_tambahan }}</td>
                        <td class="text-center">{{ $item->harga_total }}</td>
                        <td class="text-center">
                            <a href="{{ url('/admin/transaksi/proses/'.$item->id_transaksi) }}" class="btn btn-primary">{{ $item->nama_proses }}</a>
                        </td>
                        <td class="text-center">
                            <a href="{{ url('/admin/transaksi/status/'.$item->id_transaksi) }}" class="btn btn-primary">{{ $item->nama_status }}</a>
                        </td>
                        <td class="text-center">
                            <a href="{{ url('/admin/transaksi/' . $item->id_transaksi . '/delete' )}}" class="btn btn-danger">Hapus</a>
                        </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
    </div>
</div>
@endsection
