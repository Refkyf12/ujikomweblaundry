@extends('layouts.app')
@section('content')
<div class="cotainer">
<div class="card p-5">
<h2><strong>Data Status Pembayaran</strong></h2>
<a href="/admin/status/tambah" class="btn btn-primary" display="inline-block">Tambah Status Pembayaran</a>
<hr>
<table id="datatable" class="table table-bordered" style="width:100%">
                <thead>
                  <tr>
                    <th class="text-center">Nama</th>
                    <th class="text-center">Urutan</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($status as $item)
                        <tr>
                        <td class="text-center">{{ $item->nama_status }}</td>
                        <td class="text-center">{{ $item->urutan }}</td>
                        <td class="text-center">
                            <a href="/admin/status/hapus/{{ $item->id }}" class="btn btn-danger">Hapus</a>
                        </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
    </div>
</div>
@endsection
