@extends('layouts.app')
@section('content')
<body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                <strong>Tambah Pengguna</strong>
                </div>
                <div class="card-body">
                    <a href="/admin/pengguna" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    
                    <form method="post" action="/admin/pengguna/store">
 
                        {{ csrf_field() }}
 
                        <div class="form-group">
                            <label>Nama Pengguna</label>
                            <input type="text" name="name" class="form-control" placeholder="Nama pengguna ..">
 
                            @if($errors->has('nama_pengguna'))
                                <div class="text-danger">
                                    {{ $errors->first('nama_pengguna')}}
                                </div>
                            @endif
 
                        </div>

                        <div class="form-group">
                            <label>Alamat E-mail</label>
                            <input type="text" name="email" class="form-control" placeholder="Alamat E-mail ..">
 
                            @if($errors->has('email'))
                                <div class="text-danger">
                                    {{ $errors->first('email')}}
                                </div>
                            @endif
 
                        </div>
 
                        <div class="form-group">
                            <label for="role">Role</label>
                             <select class="form-control" name="role" id="role">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                            </select>
                         </div>
                         <label for="role">Note:<br> 1 = Admin<br> 2 = Kasir<br> 3 = Owner</label>

                         <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Password ..">
 
                            @if($errors->has('password'))
                                <div class="text-danger">
                                    {{ $errors->first('password')}}
                                </div>
                            @endif
 
                        </div>

                        <div class="form-group">
                            <label for="password-confirm">Konfirmasi Password</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
 
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
 
                    </form>
 
                </div>
            </div>
        </div>
    </body>
    @endsection