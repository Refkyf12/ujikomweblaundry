@extends('layouts.app')
@section('content')
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    <strong>Edit Data</strong>
                </div>
                <div class="card-body">
                    <a href="/admin/member" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    
 
                    <form method="post" action="/admin/member/update/{{ $member->id }}">
 
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
 
                        <div class="form-group">
                            <label>Nama Pelanggan</label>
                            <input type="text" name="nama_pelanggan" class="form-control" placeholder="Nama pelanggan .." value=" {{ $member->nama_pelanggan }} ">
 
                            @if($errors->has('nama_pelanggan'))
                                <div class="text-danger">
                                    {{ $errors->first('nama_pelanggan')}}
                                </div>
                            @endif
 
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" name="alamat" class="form-control" placeholder="Alamat .."  value=" {{ $member->alamat }} ">
 
                            @if($errors->has('alamat'))
                                <div class="text-danger">
                                    {{ $errors->first('alamat')}}
                                </div>
                            @endif
 
                        </div>
 
                        <div class="form-group">
                            <label for="jenis_kelamin">Jenis Kelamin</label>
                             <select class="form-control" name="jenis_kelamin" id="jenis_kelamin"  value=" {{ $member->jenis_kelamin }} ">
                                <option value= "Pria" {{ old('jenis_kelamin') == 'Pria' ? 'selected' : '' }} >Pria</option>
                                <option value= "Wanita" {{ old('jenis_kelamin') == 'Wanita' ? 'selected' : '' }} >Wanita</option>
                            </select>
                         </div>

                         <div class="form-group">
                            <label>Nomor Telepon</label>
                            <input type="text" name="no_telp" class="form-control" placeholder="Nomor Telepon .."  value=" {{ $member->no_telp }} ">
 
                            @if($errors->has('no_telp'))
                                <div class="text-danger">
                                    {{ $errors->first('no_telp')}}
                                </div>
                            @endif
 
                        </div>
 
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
 
                    </form>
 
                </div>
            </div>
        </div>
    </body>
@endsection