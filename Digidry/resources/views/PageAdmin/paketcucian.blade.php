@extends('layouts.app')
@section('content')
<div class="cotainer">
<div class="card p-5">
<h2><strong>Data Paket Cucian</strong></h2>
<a href="{{ url('/admin/paketcucian/tambah') }}" class="btn btn-primary" display="inline-block">Tambah Paket Cucian</a>
<hr>
<table id="datatable" class="table table-bordered" style="width:100%">
                <thead>
                  <tr>
                    <th class="text-center">Nama Outlet</th>
                    <th class="text-center">Jenis</th>
                    <th class="text-center">Nama Paket</th>
                    <th class="text-center">Harga</th>
                    <th class="text-center">Opsi</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($paketcucian as $item)
                        <tr>
                        <td class="text-center">{{ $item->nama_outlet }}</td>
                        <td class="text-center">{{ $item->jenis }}</td>
                        <td class="text-center">{{ $item->nama_paket }}</td>
                        <td class="text-center">{{ $item->harga }}</td>
                        <td class="text-center">
                            <a href="{{ url('admin/paketcucian/' . $item->id_paket) }}" class="btn btn-warning">Edit</a>
                            <a href="{{ url('admin/paketcucian/' . $item->id_paket . '/delete' )}}" class="btn btn-danger">Hapus</a>
                        </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
    </div>
</div>
@endsection
