              <table border="2">
                <thead>
                  <tr>
                    <th class="text-center">Nama Outlet</th>
                    <th class="text-center">Nama Pelanggan</th>
                    <th class="text-center">Nama Paket</th>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Berat</th>
                    <th class="text-center">Biaya Tambahan</th>
                    <th class="text-center">Harga Total</th>
                    <th class="text-center">Proses</th>
                    <th class="text-center">Status Pembayaran</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($transaksi as $item)
                        <tr>
                        <td class="text-center">{{ $item->nama_outlet }}</td>
                        <td class="text-center">{{ $item->nama_pelanggan }}</td>
                        <td class="text-center">{{ $item->nama_paket }}</td>
                        <td class="text-center">{{ $item->tanggal_bayar }}</td>
                        <td class="text-center">{{ $item->berat }}</td>
                        <td class="text-center">{{ $item->biaya_tambahan }}</td>
                        <td class="text-center">{{ $item->harga_total }}</td>
                        <td class="text-center">{{ $item->nama_proses }}</td>
                        <td class="text-center">{{ $item->nama_status }}</td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
