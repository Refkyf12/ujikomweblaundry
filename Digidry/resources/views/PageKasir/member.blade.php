@extends('layouts.appKasir')
@section('content')
<div class="cotainer">
<div class="card p-5">
<h2><strong>Data Pelanggan</strong></h2>
<a href="/kasir/member/tambah" class="btn btn-primary" display="inline-block">Tambah Pelanggan</a>
<hr>
<table id="datatable" class="table table-bordered" style="width:100%">
                <thead>
                  <tr>
                    <th class="text-center">Nama Pelanggan</th>
                    <th class="text-center">Alamat</th>
                    <th class="text-center">Jenis Kelamin</th>
                    <th class="text-center">Nomor Telepon</th>
                    <th class="text-center">Opsi</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($member as $item)
                        <tr>
                        <td class="text-center">{{ $item->nama_pelanggan }}</td>
                        <td class="text-center">{{ $item->alamat }}</td>
                        <td class="text-center">{{ $item->jenis_kelamin }}</td>
                        <td class="text-center">{{ $item->no_telp }}</td>
                        <td class="text-center">
                            <a href="/kasir/member/edit/{{ $item->id }}" class="btn btn-warning">Edit</a>
                            <a href="/kasir/member/hapus/{{ $item->id }}" class="btn btn-danger">Hapus</a>
                        </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
    </div>
</div>
@endsection
