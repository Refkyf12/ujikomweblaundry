@extends('layouts.appKasir')
@section('content')
<body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                <strong>Manage Data Transaksi</strong>
                </div>
                <div class="card-body">
                    <a href="/kasir/transaksi" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>

                   @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                             @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                             @endforeach
                             </ul>
                        </div>
                    @endif

                    <form action=" {{ url('/kasir/transaksi' ,@$transaksi->id_transaksi) }} " method="POST">
 
                    {{ csrf_field() }} 
 
                        <div class="form-group">
                            <label for="id_outlet">Nama Outlet</label>
                             <select class="form-control" name="id_outlet" id="id_outlet">
                             @foreach($c_outlet as $row)
                                <option value="{{ old('id_outlet' ,@$row->id_outlet) }}">{{ @$row->nama_outlet }}</option>
                            @endforeach
                            </select>
                         </div>

                         <div class="form-group">
                            <label for="id_member">Nama pelanggan</label>
                             <select class="form-control" name="id_member" id="id_member">
                             @foreach($c_pelanggan as $row)
                                <option value="{{ old('id_member' ,@$row->id) }}">{{ @$row->nama_pelanggan }}</option>
                            @endforeach
                            </select>
                         </div>

                         <div class="form-group">
                            <label for="id_paket">Nama paket cucian</label>
                             <select class="form-control" name="id_paket" id="id_paket">
                             @foreach($c_paket as $row)
                                <option value="{{ old('id_paket' ,@$row->id_paket) }}">{{ @$row->nama_paket }}</option>
                            @endforeach
                            </select>
                         </div>

                        <div class="form-group">
                            <label>Berat</label>
                            <input type="text" name="berat" class="form-control" placeholder="Berat .." value=" {{ old('berat', @$transaksi->berat) }} ">
                        </div>

                        <div class="form-group">
                            <label>Biaya tambahan</label>
                            <input type="text" name="biaya_tambahan" class="form-control" placeholder="Biaya tambahan .." value=" {{ old('biaya_tambahan', @$transaksi->biaya_tambahan) }} ">
                        </div>
 
                        <div class="form-group">
                            <label for="id_proses">Proses Cucian</label>
                             <select class="form-control" name="id_proses" id="id_proses">
                             @foreach($c_proses as $row)
                                <option value="{{ old('id_proses' ,@$row->id_proses) }}">{{ @$row->nama_proses }}</option>
                            @endforeach
                            </select>
                         </div>

                         <div class="form-group">
                            <label for="id_status">Proses Cucian</label>
                             <select class="form-control" name="id_status" id="id_status">
                             @foreach($c_status as $row)
                                <option value="{{ old('id_status' ,@$row->id_status) }}">{{ @$row->nama_status }}</option>
                            @endforeach
                            </select>
                         </div>

                         <div class="form-group">
                            <input type="submit" class="btn btn-success" value="submit">
                        </div>

                    </form>   
                </div>
            </div>
        </div>
    </body>
    @endsection