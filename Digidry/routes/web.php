<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['prefix' => '/admin', 'middleware' => ['auth', 'admin']], function () {
    //Data Pelanggan
Route::get("/member", "MemberController@showDataMember");
Route::get('/member/tambah', 'MemberController@addDataMember');
Route::post('/member/store', 'MemberController@store');
Route::get('/member/edit/{id}', 'MemberController@editDataMember');
Route::put('/member/update/{id}', 'MemberController@update');
Route::get('/member/hapus/{id}', 'MemberController@delete');

    //Data Outlet
Route::get("/outlet", "OutletController@showDataOutlet");
Route::get('/outlet/tambah', 'OutletController@addDataOutlet');
Route::post('/outlet/store', 'OutletController@store');  
Route::get('/outlet/edit/{id}', 'OutletController@editDataOutlet');
Route::put('/outlet/update/{id}', 'OutletController@update');
Route::get('/outlet/hapus/{id}', 'OutletController@delete');

    //Data Paket Cucian    
Route::get("/paketcucian", "PaketCucianController@showDataPaketCucian");
Route::post('/paketcucian', 'PaketCucianController@store');
Route::get('/paketcucian/tambah', 'PaketCucianController@addDataPaketCucian');
Route::get('/paketcucian/{id}', 'PaketCucianController@editDataPaketCucian');
Route::post('/paketcucian/{id}', 'PaketCucianController@update');
Route::get('/paketcucian/{id}/delete', 'PaketCucianController@destroy');

    //Data Pengguna
Route::get("/pengguna", "PenggunaController@showDataPengguna");
Route::get('/pengguna/tambah', 'PenggunaController@addDataPengguna');
Route::post('/pengguna/store', 'PenggunaController@store');
Route::get('/pengguna/edit/{id}', 'PenggunaController@editDataPengguna');
Route::put('/pengguna/update/{id}', 'PenggunaController@update');
Route::get('/pengguna/hapus/{id}', 'PenggunaController@delete');

    //Data Transaksi
Route::get('/transaksi', 'TransaksiController@showDataTransaksi');
Route::post('/transaksi', 'TransaksiController@store');
Route::get('/transaksi/tambah', 'TransaksiController@addDataTransaksi');
Route::get('/transaksi/proses/{id}','TransaksiController@proses');
Route::get('/transaksi/status/{id}','TransaksiController@status');
Route::get('/transaksi/laporan', 'TransaksiController@printLaporan');
Route::get('/transaksi/{id}/delete', 'TransaksiController@destroyAdmin');

    //Data Status
Route::get("/status", "TransaksiController@showDataStatus");
Route::get('/status/tambah', 'TransaksiController@addDataStatus');
Route::post('/status/store', 'TransaksiController@storeStatus');
Route::get('/status/hapus/{id}', 'PenggunaController@deleteStatus');

    //Data Proses
Route::get("/proses", "TransaksiController@showDataProses");
Route::get('/proses/tambah', 'TransaksiController@addDataProses');
Route::post('/proses/store', 'TransaksiController@storeProses');
Route::get('/proses/hapus/{id}', 'PenggunaController@deleteProses');

});

Route::group(['prefix' => '/kasir', 'middleware' => ['auth', 'kasir']], function () {
    //Data Pelanggan
Route::get("/member", "MemberController@showDataMemberKasir");
Route::get('/member/tambah', 'MemberController@addDataMemberKasir');
Route::post('/member/store', 'MemberController@storeKasir');
Route::get('/member/edit/{id}', 'MemberController@editDataMemberKasir');
Route::put('/member/update/{id}', 'MemberController@updateKasir');
Route::get('/member/hapus/{id}', 'MemberController@deleteKasir');

Route::get('/transaksi', 'TransaksiController@showDataTransaksiKasir');
Route::post('/transaksi', 'TransaksiController@storeKasir');
Route::get('/transaksi/tambah', 'TransaksiController@addDataTransaksiKasir');
Route::get('/transaksi/proses/{id}','TransaksiController@prosesKasir');
Route::get('/transaksi/status/{id}','TransaksiController@statusKasir');
Route::get('/transaksi/laporan', 'TransaksiController@printLaporan');
Route::get('/transaksi/{id}/delete', 'TransaksiController@destroyKasir');
});

Route::group(['prefix' => '/owner', 'middleware' => ['auth', 'owner']], function () {

Route::get('/transaksi', 'TransaksiController@showDataTransaksiOwner');
Route::get('/transaksi/laporan', 'TransaksiController@printLaporan');
});