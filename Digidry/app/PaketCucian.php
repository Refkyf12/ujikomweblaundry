<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaketCucian extends Model
{
    protected $table = "paket_cucian";

    public $fillable = 
    [
        "id_paket",
        "id_outlet",
        "jenis",
        "nama_paket", 
        "harga"
    ];

    public $primaryKey = "id_paket";

    protected $casts = ['id_paket' => 'int'];

    public $timestamps = true;

    public function outlet()
    {
    return $this->belongsTo('App\Outlet', 'id_outlet');
    }
}
