<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proses extends Model
{
    protected $table = "proses";

    public $fillable = 
    [
        "id_proses",
        "nama_proses",
        "urutan"
    ];

    public $primaryKey = "id_proses";

    protected $casts = ['id_proses' => 'int'];

    public $timestamps = true;
}
