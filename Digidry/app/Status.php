<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = "status";

    public $fillable = 
    [
        "id_status",
        "nama_status",
        "urutan"
    ];

    public $primaryKey = "id_status";

    protected $casts = ['id_status' => 'int'];

    public $timestamps = true;
}
