<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outlet extends Model
{
    protected $table = "outlet";

    public $fillable = 
    [
        "id_outlet",
        "nama_outlet",
        "alamat", 
        "no_telp"
    ];

    public $primaryKey = "id_outlet";

    protected $casts = ['id_outlet' => 'int'];

    public $timestamps = true;

    public function paketcucian()
    {
    return $this->hasMany('App\PaketCucian');
    }
}
