<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaketCucian;

class PaketCucianController extends Controller
{
    public function showDataPaketCucian(){
        $paketcucian = \DB::table('paket_cucian')
        ->join('outlet','outlet.id_outlet','=','paket_cucian.id_outlet')
        ->get();
        return view("pageAdmin.paketcucian", compact('paketcucian'));
    }
    public function addDataPaketCucian(){
        $combo = \DB::table('outlet')->get();
        return view('pageAdmin.paketcucian_tambah', compact('paketcucian','combo'));

    }

    public function store(Request $request){

        $input = $request->all();

        unset($input['_token']);
        $status =\DB::table('paket_cucian')->insert($input);

        if($status){
            return redirect("/admin/paketcucian")->with('success', " Data berhasil di input");
        }else{
            return redirect('/paketcucian/create')->with('error', " Data gagal di input");
        }
    }

    public function editDataPaketCucian($id_paket){
        // $paketcucian = PaketCucian::find($id);
        $paketcucian = \DB::table('paket_cucian')->where('id_paket', $id_paket)->first();
        $combo = \DB::table('outlet')->get();
        // return view('PageAdmin.paketcucian_tambah', ['paket_cucian' => $paketcucian], compact('combo'));
        return view("pageAdmin.paketcucian_tambah", compact('paketcucian','combo'));
    }
    public function update(Request $request, $id_paket){
        $paketcucian = \DB::table('paket_cucian')->where('id_paket',$id_paket)->update([
            'id_outlet' => $request->id_outlet,
            'jenis' => $request->jenis,
            'nama_paket' => $request->nama_paket,
            'harga' => $request->harga
        ]);
        return redirect("/admin/paketcucian");
    }
    public function destroy(Request $request, $id_paket){

        $result = \DB::table('paket_cucian')->where('id_paket', $id_paket)->delete();
        if($result) return redirect()->back()->with('success', 'Data Berhasil Dihapus');
        else return view('PageAdmin.paketcucian')->with('error', 'Data Gagal Dihapus');
    }
}
