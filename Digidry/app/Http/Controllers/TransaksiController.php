<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaketCucian;
use App\Transaksi;
use App\Status;
use App\Proses;
use App\Outlet;
use PDF;

class TransaksiController extends Controller
{
    public function showDataTransaksi()
    {
        $data['transaksi'] = \DB::table('transaksi')
        ->join('outlet','outlet.id_outlet','=','transaksi.id_outlet')
        ->join('pelanggan_tables','pelanggan_tables.id','=','transaksi.id_member')
        ->join('paket_cucian','paket_cucian.id_paket','=','transaksi.id_paket')
        ->join('status','status.id_status','=','transaksi.id_status')
        ->join('proses','proses.id_proses','=','transaksi.id_proses')
        ->get();
        // dd($data);
        return view("pageAdmin.transaksi",$data);
    }
    public function addDataTransaksi(){
        $c_pelanggan = \DB::table('pelanggan_tables')->get();
        $c_outlet = \DB::table('outlet')->get();
        $c_paket = \DB::table('paket_cucian')->get();
        $c_status = \DB::table('status')->get();
        $c_proses = \DB::table('proses')->get();
        // dd($combo);
        return view("pageAdmin.transaksi_tambah", compact('c_pelanggan', 'c_outlet', 'c_paket', 'c_status', 'c_proses', 'transaksi'));
    }
    public function store(Request $request){

        $this->validate($request,[
            'biaya_tambahan' => 'required',
    		'berat'=>'required'
        ]);
        
        $data['id_outlet'] = $request->id_outlet;
    	$data['id_member'] = $request->id_member;
        $data['id_paket'] = $request->id_paket;
        $data['berat'] = $request->berat;
        $data['biaya_tambahan'] = $request->biaya_tambahan;
    	$data['id_status'] = $request->id_status;
        $data['id_proses'] = $request->id_proses;

    	$harga = PaketCucian::find($request->id_paket);
    	$harga_paket = $harga->harga;
        $berat = $request->berat;
        $biaya_tambahan = $request->biaya_tambahan;

    	$harga_total = ($harga_paket * $berat) + $biaya_tambahan;

        $data['harga_total'] = $harga_total;
        

        $status = \DB::table('transaksi')->insert($data);
        

    	if($status){
            return redirect("/admin/transaksi")->with('success', " Data berhasil di input");
        }else{
            return redirect('admin/transaksi/tambah')->with('error', " Data gagal di input");
        }
    }

    public function status($id_transaksi){
        try {
            $transaksi = Transaksi::find($id_transaksi);
            $id_status = $transaksi->id_status;
            $urutan_status = $transaksi->status_status->urutan;

            $urutan_baru = $urutan_status + 1;
            $status_baru = Status::where('urutan',$urutan_baru)->first();

            Transaksi::where('id_transaksi',$id_transaksi)->update([
                'id_status'=>$status_baru->id_status
            ]);

            \Session::flash('sukses','Status berhasil dinaikkan');

        } catch (\Exception $e) {
            \Session::flash('gagal',$e->getMessage());
        }

        return redirect()->back();
    }

    public function proses($id_transaksi){
        try {
            $transaksi = Transaksi::find($id_transaksi);
            $id_proses = $transaksi->id_proses;
            $urutan_proses = $transaksi->status_proses->urutan;

            $urutan_baru = $urutan_proses + 1;
            $proses_baru = Proses::where('urutan',$urutan_baru)->first();

            Transaksi::where('id_transaksi',$id_transaksi)->update([
                'id_proses'=>$proses_baru->id_proses
            ]);

            \Session::flash('sukses','Proses berhasil dinaikkan');

        } catch (\Exception $e) {
            \Session::flash('gagal',$e->getMessage());
        }

        return redirect()->back();
    }

    public function showDataTransaksiKasir()
    {
        $data['transaksi'] = \DB::table('transaksi')
        ->join('outlet','outlet.id_outlet','=','transaksi.id_outlet')
        ->join('pelanggan_tables','pelanggan_tables.id','=','transaksi.id_member')
        ->join('paket_cucian','paket_cucian.id_paket','=','transaksi.id_paket')
        ->join('status','status.id_status','=','transaksi.id_status')
        ->join('proses','proses.id_proses','=','transaksi.id_proses')
        ->get();
        // dd($data);
        return view("pageKasir.transaksi",$data);
    }
    public function addDataTransaksiKasir(){
        $c_pelanggan = \DB::table('pelanggan_tables')->get();
        $c_outlet = \DB::table('outlet')->get();
        $c_paket = \DB::table('paket_cucian')->get();
        $c_status = \DB::table('status')->get();
        $c_proses = \DB::table('proses')->get();
        // dd($combo);
        return view("pageKasir.transaksi_tambah", compact('c_pelanggan', 'c_outlet', 'c_paket', 'c_status', 'c_proses', 'transaksi'));
    }
    public function storeKasir(Request $request){

        $this->validate($request,[
            'biaya_tambahan' => 'required',
    		'berat'=>'required'
        ]);
        
        $data['id_outlet'] = $request->id_outlet;
    	$data['id_member'] = $request->id_member;
        $data['id_paket'] = $request->id_paket;
        $data['berat'] = $request->berat;
        $data['biaya_tambahan'] = $request->biaya_tambahan;
    	$data['id_status'] = $request->id_status;
        $data['id_proses'] = $request->id_proses;

    	$harga = PaketCucian::find($request->id_paket);
    	$harga_paket = $harga->harga;
        $berat = $request->berat;
        $biaya_tambahan = $request->biaya_tambahan;

    	$harga_total = ($harga_paket * $berat) + $biaya_tambahan;

        $data['harga_total'] = $harga_total;
        

        $status = \DB::table('transaksi')->insert($data);
        

    	if($status){
            return redirect("/kasir/transaksi")->with('success', " Data berhasil di input");
        }else{
            return redirect('/kasir/transaksi/tambah')->with('error', " Data gagal di input");
        }
    }

    public function statusKasir($id_transaksi){
        try {
            $transaksi = Transaksi::find($id_transaksi);
            $id_status = $transaksi->id_status;
            $urutan_status = $transaksi->status_status->urutan;

            $urutan_baru = $urutan_status + 1;
            $status_baru = Status::where('urutan',$urutan_baru)->first();

            Transaksi::where('id_transaksi',$id_transaksi)->update([
                'id_status'=>$status_baru->id_status
            ]);

            \Session::flash('sukses','Status berhasil dinaikkan');

        } catch (\Exception $e) {
            \Session::flash('gagal',$e->getMessage());
        }

        return redirect()->back();
    }

    public function prosesKasir($id_transaksi){
        try {
            $transaksi = Transaksi::find($id_transaksi);
            $id_proses = $transaksi->id_proses;
            $urutan_proses = $transaksi->status_proses->urutan;

            $urutan_baru = $urutan_proses + 1;
            $proses_baru = Proses::where('urutan',$urutan_baru)->first();

            Transaksi::where('id_transaksi',$id_transaksi)->update([
                'id_proses'=>$proses_baru->id_proses
            ]);

            \Session::flash('sukses','Proses berhasil dinaikkan');

        } catch (\Exception $e) {
            \Session::flash('gagal',$e->getMessage());
        }

        return redirect()->back();
    }
    public function showDataTransaksiOwner()
    {
        $data['transaksi'] = \DB::table('transaksi')
        ->join('outlet','outlet.id_outlet','=','transaksi.id_outlet')
        ->join('pelanggan_tables','pelanggan_tables.id','=','transaksi.id_member')
        ->join('paket_cucian','paket_cucian.id_paket','=','transaksi.id_paket')
        ->join('status','status.id_status','=','transaksi.id_status')
        ->join('proses','proses.id_proses','=','transaksi.id_proses')
        ->get();
        // dd($data);
        return view("PageOwner.transaksi",$data);
    }
    public function printLaporan()
    {
        $data['transaksi'] = \DB::table('transaksi')
        ->join('outlet','outlet.id_outlet','=','transaksi.id_outlet')
        ->join('pelanggan_tables','pelanggan_tables.id','=','transaksi.id_member')
        ->join('paket_cucian','paket_cucian.id_paket','=','transaksi.id_paket')
        ->join('status','status.id_status','=','transaksi.id_status')
        ->join('proses','proses.id_proses','=','transaksi.id_proses')
        ->get();
        
        $pdf = PDF::loadview("PageLaporan/laporan", $data);
        return $pdf->download('laporan-transaksi.pdf');
    }

    public function showDataStatus()
    {
        $status =  Status::all();
        return view('PageAdmin.status', compact("status"));
    }
    public function addDataStatus()
    {
    	return view('PageAdmin.status_tambah');
    }
    public function storeStatus(Request $request)
    {
    	$this->validate($request,[
    		'nama_status' => 'required',
            'urutan' => 'required',
    	]);
 
        Status::create([
    		'nama_status' => $request->nama_status,
            'urutan' => $request->urutan
    	]);
 
    	return redirect('/admin/status');
    }

    public function showDataProses()
    {
        $proses =  Proses::all();
        return view('PageAdmin.proses', compact("proses"));
    }
    public function addDataProses()
    {
    	return view('PageAdmin.proses_tambah');
    }
    public function storeProses(Request $request)
    {
    	$this->validate($request,[
    		'nama_proses' => 'required',
            'urutan' => 'required',
    	]);
 
        Proses::create([
    		'nama_proses' => $request->nama_proses,
            'urutan' => $request->urutan
    	]);
 
    	return redirect('/admin/proses');
    }

    public function deleteStatus($id)
    {
    $status = Status::find($id);
    $status->delete();
    return redirect('/admin/status');
    }

    public function deleteProses($id)
    {
    $proses = Proses::find($id);
    $proses->delete();
    return redirect('/admin/proses');
    }

    public function destroyAdmin(Request $request, $id_transaksi){

        $result = \DB::table('transaksi')->where('id_transaksi', $id_transaksi)->delete();
        if($result) return redirect()->back()->with('success', 'Data Berhasil Dihapus');
        else return view('PageAdmin.paketcucian')->with('error', 'Data Gagal Dihapus');
    }

    public function destroyKasir(Request $request, $id_transaksi){

        $result = \DB::table('transaksi')->where('id_transaksi', $id_transaksi)->delete();
        if($result) return redirect()->back()->with('success', 'Data Berhasil Dihapus');
        else return view('PageAdmin.paketcucian')->with('error', 'Data Gagal Dihapus');
    }
}
