<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class MemberController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showDataMember()
    {
        $member =  Member::all();
        return view('PageAdmin.member', compact("member"));
    }
    public function addDataMember()
    {
    	return view('PageAdmin.member_tambah');
    }
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'nama_pelanggan' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
    	]);
 
        Member::create([
    		'nama_pelanggan' => $request->nama_pelanggan,
            'alamat' => $request->alamat,
            'jenis_kelamin' => $request->jenis_kelamin,
            'no_telp' => $request->no_telp
    	]);
 
    	return redirect('/admin/member');
    }
    public function editDataMember($id)
    {
        $member = Member::find($id);
        return view('PageAdmin.member_edit', ['member' => $member]);
    }
    public function update($id, Request $request)
    {
        $this->validate($request,[
	        'nama_pelanggan' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
    ]);
 
        $member = Member::find($id);
        $member->nama_pelanggan = $request->nama_pelanggan;
        $member->alamat = $request->alamat;
        $member->jenis_kelamin = $request->jenis_kelamin;
        $member->no_telp = $request->no_telp;
        $member->save();
        return redirect('/admin/member');
    }
    public function delete($id)
    {
    $member = Member::find($id);
    $member->delete();
    return redirect('/admin/member');
    }
    
    //Kasir

    public function showDataMemberKasir()
    {
        $member =  Member::all();
        return view('PageKasir.member', compact("member"));
    }
    public function addDataMemberKasir()
    {
    	return view('PageKasir.member_tambah');
    }
    public function storeKasir(Request $request)
    {
    	$this->validate($request,[
    		'nama_pelanggan' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
    	]);
 
        Member::create([
    		'nama_pelanggan' => $request->nama_pelanggan,
            'alamat' => $request->alamat,
            'jenis_kelamin' => $request->jenis_kelamin,
            'no_telp' => $request->no_telp
    	]);
 
    	return redirect('/admin/member');
    }
    public function editDataMemberKasir($id)
    {
        $member = Member::find($id);
        return view('PageKasir.member_edit', ['member' => $member]);
    }
    public function updateKasir($id, Request $request)
    {
        $this->validate($request,[
	        'nama_pelanggan' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
    ]);
 
        $member = Member::find($id);
        $member->nama_pelanggan = $request->nama_pelanggan;
        $member->alamat = $request->alamat;
        $member->jenis_kelamin = $request->jenis_kelamin;
        $member->no_telp = $request->no_telp;
        $member->save();
        return redirect('/admin/member');
    }
    public function deleteKasir($id)
    {
    $member = Member::find($id);
    $member->delete();
    return redirect('/admin/member');
    }   
}
