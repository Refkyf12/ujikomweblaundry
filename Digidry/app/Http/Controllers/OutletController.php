<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Outlet;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class OutletController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showDataOutlet()
    {
        $outlet =  Outlet::all();
        return view('PageAdmin.outlet', compact("outlet"));
    }
    public function addDataOutlet()
    {
    	return view('PageAdmin.outlet_tambah');
    }
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'nama_outlet' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
    	]);
 
        Outlet::create([
    		'nama_outlet' => $request->nama_outlet,
            'alamat' => $request->alamat,
            'no_telp' => $request->no_telp
    	]);
 
    	return redirect('/admin/outlet');
    }
    public function editDataOutlet($id)
    {
        $outlet = Outlet::find($id);
        return view('PageAdmin.outlet_edit', ['outlet' => $outlet]);
    }
    public function update($id, Request $request)
    {
        $this->validate($request,[
	        'nama_outlet' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
    ]);
 
        $outlet = Outlet::find($id);
        $outlet->nama_outlet = $request->nama_outlet;
        $outlet->alamat = $request->alamat;
        $outlet->no_telp = $request->no_telp;
        $outlet->save();
        return redirect('/admin/outlet');
    }
    public function delete($id)
    {
    $outlet = Outlet::find($id);
    $outlet->delete();
    return redirect('/admin/outlet');
    }    
    
    public function paketcucian()
    {
    return $this->hasMany('App\PaketCucian');
    }
   
}

