<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class PenggunaController extends Controller
{
    public function showDataPengguna()
    {
        $pengguna =  User::all();
        return view('PageAdmin.pengguna', compact("pengguna"));
    }
    public function addDataPengguna()
    {
    	return view('PageAdmin.pengguna_tambah');
    }
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
    	]);
 
        User::create([
    		'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request['password']),
            'role' => $request->role
    	]);
 
    	return redirect('/admin/pengguna');
    }
    public function editDataPengguna($id)
    {
        $pengguna = User::find($id);
        return view('PageAdmin.pengguna_edit', ['pengguna' => $pengguna]);
    }
    public function update($id, Request $request)
    {
        $this->validate($request,[
	        'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
    ]);
 
        $pengguna = User::find($id);
        $pengguna->name = $request->name;
        $pengguna->email = $request->email;
        $pengguna->role = $request->role;
        $pengguna->password = Hash::make($request['password']);
        $pengguna->save();
        return redirect('/admin/pengguna');
    }
    public function delete($id)
    {
    $pengguna = User::find($id);
    $pengguna->delete();
    return redirect('/admin/pengguna');
    }
}
