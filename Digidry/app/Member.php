<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = "pelanggan_tables";

    public $fillable = 
    [
        "id",
        "nama_pelanggan",
        "alamat",
        "jenis_kelamin", 
        "no_telp"
    ];

    public $primaryKey = "id";

    protected $casts = ['id' => 'int'];

    public $timestamps = true;
}
