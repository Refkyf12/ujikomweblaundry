<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = "transaksi";

    public $fillable = 
    [
        "id_transaksi",
        "id_outlet",
        "id_member", 
        "id_paket",
        "tanggal_bayar",
        "berat",
        "harga_total",
        "biaya_tambahan",
        "id_proses",
        "id_status",
    ];

    public $primaryKey = "id_transaksi";

    protected $casts = ['id_transaksi' => 'int'];

    public function status_status(){
        return $this->belongsTo('App\Status','id_status','id_status');
    }
    public function status_proses(){
        return $this->belongsTo('App\Proses','id_proses','id_proses');
    }
}
