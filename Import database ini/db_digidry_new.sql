-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2020 at 08:25 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_digidry`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_04_11_080809_create_pelanggan_tables', 1),
(4, '2020_04_11_123327_create_outlet_table', 2),
(5, '2020_04_11_135210_create_paket_cucian_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `outlet`
--

CREATE TABLE `outlet` (
  `id_outlet` bigint(20) NOT NULL,
  `nama_outlet` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `outlet`
--

INSERT INTO `outlet` (`id_outlet`, `nama_outlet`, `alamat`, `no_telp`, `created_at`, `updated_at`) VALUES
(3, 'Launderia', 'Rancamanyar', '081322082517', '2020-04-11 07:36:58', '2020-04-11 07:36:58'),
(4, 'Lavander', 'Mars', '0897654561', '2020-04-11 08:29:18', '2020-04-11 08:29:18');

-- --------------------------------------------------------

--
-- Table structure for table `paket_cucian`
--

CREATE TABLE `paket_cucian` (
  `id_paket` bigint(20) NOT NULL,
  `id_outlet` bigint(20) NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_paket` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `paket_cucian`
--

INSERT INTO `paket_cucian` (`id_paket`, `id_outlet`, `jenis`, `nama_paket`, `harga`, `created_at`, `updated_at`) VALUES
(2, 3, 'Bantal', 'Paket Bantal', 20000, NULL, NULL),
(3, 4, 'Selimut', 'Paket Selimut', 10000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan_tables`
--

CREATE TABLE `pelanggan_tables` (
  `id` bigint(20) NOT NULL,
  `nama_pelanggan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pelanggan_tables`
--

INSERT INTO `pelanggan_tables` (`id`, `nama_pelanggan`, `alamat`, `jenis_kelamin`, `no_telp`, `created_at`, `updated_at`) VALUES
(1, 'Refky Fauzi', 'Permata Kopo 3', 'Pria', '081322082517', NULL, '2020-04-11 05:12:38'),
(2, 'Farhan Dwi Anggara', 'Rancamanyar', 'Wanita', '081322082517', '2020-04-12 07:55:14', '2020-04-12 07:55:14');

-- --------------------------------------------------------

--
-- Table structure for table `proses`
--

CREATE TABLE `proses` (
  `id_proses` bigint(20) NOT NULL,
  `nama_proses` varchar(30) NOT NULL,
  `urutan` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proses`
--

INSERT INTO `proses` (`id_proses`, `nama_proses`, `urutan`, `created_at`, `updated_at`) VALUES
(1, 'Belum diproses', 1, '2020-04-13 10:30:43', '0000-00-00 00:00:00'),
(2, 'Sudah Diproses', 2, '2020-04-15 07:06:21', '0000-00-00 00:00:00'),
(3, 'Beres', 3, '2020-04-15 10:51:03', '2020-04-15 10:51:03');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id_status` bigint(20) NOT NULL,
  `nama_status` varchar(30) NOT NULL,
  `urutan` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id_status`, `nama_status`, `urutan`, `created_at`, `updated_at`) VALUES
(2, 'Belum dibayar', 1, '2020-04-13 10:30:30', '0000-00-00 00:00:00'),
(3, 'Sudah Dibayar', 2, '2020-04-13 14:28:32', '0000-00-00 00:00:00'),
(4, 'Lunas', 3, '2020-04-15 10:25:27', '2020-04-15 10:25:27');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` bigint(20) NOT NULL,
  `id_outlet` bigint(20) NOT NULL,
  `id_member` bigint(20) NOT NULL,
  `id_paket` bigint(20) NOT NULL,
  `tanggal_bayar` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `berat` int(11) NOT NULL,
  `harga_total` int(11) NOT NULL,
  `biaya_tambahan` int(20) DEFAULT NULL,
  `id_status` bigint(20) NOT NULL,
  `id_proses` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_outlet`, `id_member`, `id_paket`, `tanggal_bayar`, `berat`, `harga_total`, `biaya_tambahan`, `id_status`, `id_proses`, `created_at`, `updated_at`) VALUES
(2, 4, 2, 3, '2020-04-15 17:57:40', 10, 102000, 2000, 4, 3, '2020-04-15 17:57:53', '2020-04-15 10:57:53'),
(3, 4, 1, 3, '2020-04-15 18:04:05', 20, 200000, 0, 3, 2, '2020-04-15 18:04:11', '2020-04-15 11:04:11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(3) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Refky Fauzi', 'refky.f1212@gmail.com', NULL, '$2y$10$RUspJ14carCBJSKuCvfDoexgtRN/dV0LA5xpMl6D8aRf6IfTOXsRK', 1, NULL, '2020-04-11 01:12:24', '2020-04-12 21:42:03'),
(5, 'Admin', 'admin@mail.id', NULL, '$2y$10$yNQ6hVutl7AbGswlCYiDEOtQBhYR2F0s5lVWzkVLjxI7SWEDyTNZe', 1, NULL, '2020-04-12 21:37:48', '2020-04-12 21:37:48'),
(6, 'Kasir', 'kasir@mail.id', NULL, '$2y$10$96YRL8EQjBGMnVTwuvcB5elmiR4HPk4hWN.c.zCiQaecXlHmYzFb2', 2, NULL, '2020-04-12 21:49:27', '2020-04-12 21:49:27'),
(7, 'Owner', 'owner@mail.id', NULL, '$2y$10$tZsBbPxyYmCJBE7zYTjoFOM7PcEO4jYuT3Cxv4eyyF0sKI70S8Jkq', 3, NULL, '2020-04-15 01:31:52', '2020-04-15 01:31:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outlet`
--
ALTER TABLE `outlet`
  ADD PRIMARY KEY (`id_outlet`);

--
-- Indexes for table `paket_cucian`
--
ALTER TABLE `paket_cucian`
  ADD PRIMARY KEY (`id_paket`),
  ADD KEY `id_outlet` (`id_outlet`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pelanggan_tables`
--
ALTER TABLE `pelanggan_tables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proses`
--
ALTER TABLE `proses`
  ADD PRIMARY KEY (`id_proses`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `FK_TRANSAKSIOUTLET` (`id_outlet`),
  ADD KEY `FK_TRANSAKSIMEMBER` (`id_member`),
  ADD KEY `FK_TRANSAKSIPAKET` (`id_paket`),
  ADD KEY `FK_STATUS` (`id_status`),
  ADD KEY `FK_PROSES` (`id_proses`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `outlet`
--
ALTER TABLE `outlet`
  MODIFY `id_outlet` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `paket_cucian`
--
ALTER TABLE `paket_cucian`
  MODIFY `id_paket` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pelanggan_tables`
--
ALTER TABLE `pelanggan_tables`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `proses`
--
ALTER TABLE `proses`
  MODIFY `id_proses` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id_status` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `paket_cucian`
--
ALTER TABLE `paket_cucian`
  ADD CONSTRAINT `FK_PAKETOUTLET` FOREIGN KEY (`id_outlet`) REFERENCES `outlet` (`id_outlet`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `FK_PROSES` FOREIGN KEY (`id_proses`) REFERENCES `proses` (`id_proses`),
  ADD CONSTRAINT `FK_STATUS` FOREIGN KEY (`id_status`) REFERENCES `status` (`id_status`),
  ADD CONSTRAINT `FK_TRANSAKSIMEMBER` FOREIGN KEY (`id_member`) REFERENCES `pelanggan_tables` (`id`),
  ADD CONSTRAINT `FK_TRANSAKSIOUTLET` FOREIGN KEY (`id_outlet`) REFERENCES `outlet` (`id_outlet`),
  ADD CONSTRAINT `FK_TRANSAKSIPAKET` FOREIGN KEY (`id_paket`) REFERENCES `paket_cucian` (`id_paket`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
